package com.example.gitlabkotlinsast

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GitlabKotlinSastApplication

fun main(args: Array<String>) {
    runApplication<GitlabKotlinSastApplication>(*args)
}
